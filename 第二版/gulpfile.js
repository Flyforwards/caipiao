var gulp = require('gulp'),
	sass = require('gulp-ruby-sass'),
	rename = require('gulp-rename'),
	cssmin = require('gulp-cssmin'),
	jsmin = require('gulp-jsmin'),
	htmlmin = require('gulp-htmlmin'),
	rjs = require('gulp-requirejs'),
	shell = require('gulp-shell'),
	imagemin = require('gulp-imagemin'),
	//imagemin = require('gulp-tinypng'),
	pngquant = require('imagemin-pngquant'),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	concatCss = require('gulp-concat-css'),
	replace = require('gulp-replace'),
	copy2 = require('gulp-copy2'),
	uglify = require('gulp-uglify'),
	spriter=require('gulp-css-spriter'),
	clean = require('gulp-clean')


// 合并公共样式
gulp.task('concatCommoncss',function(){
	return gulp.src(['./caipiao/src/styles/common.css','./caipiao/src/styles/style.css','./caipiao/src/styles/csscommon/**/*.css'])
		.pipe(autoprefixer())
		.pipe(concat('bundle.css'))
		.pipe(rename({suffix : '.min'}))
		.pipe(replace(/..\/..\/images\//ig,'.\/..\/images\/'))
		.pipe(cssmin())
		.pipe(gulp.dest('./build/styles/'))
});

gulp.task('cssmin0',function(){
	return gulp.src(['./caipiao/src/styles/*.css'])
		.pipe(autoprefixer())
		.pipe(cssmin())
		.pipe(gulp.dest('./build/styles/'))
})
/*gulp.task('cleanFile',['jsmin'], function () {
  return gulp.src(['./build/js/src/plugins'], {read: false})
    	 .pipe(clean())
});*/
gulp.task('cleanFile',['jsmin'], function () {
  return gulp.src(['./build/js/src/plugins'], {read: false})
    	 .pipe(clean())
});
gulp.task('imagemins',function(){
	return gulp.src('./caipiao/images/**/*')
	.pipe(imagemin({
		progressive: true,
		svgoPlugins: [{removeViewBox: false}],
		use: [pngquant()]
	}))
	.pipe(gulp.dest('./build/images'));
});
// build----html
gulp.task("replaceHtmlStylePaths",function(){
	return gulp.src('./caipiao/src/tpl/**/*.html')
		.pipe(replace(/<link rel="stylesheet" href="..\/styles\/csscommon\/reset.css">/g, '<link rel="stylesheet" href="..\/styles\/bundle.min.css">'))
		.pipe(replace(/<link rel="stylesheet" href="..\/styles\/common.css">/g, ' '))
		.pipe(replace(/<link rel="stylesheet" href="..\/styles\/style.css">/g, ' '))
		.pipe(replace(/<script src="..\/..\/src\/scripts\/login\/jquery.min.js"><\/script>/g, '<script src="..\/scripts\/xeplugins.min.js"><\/script>'))
		.pipe(replace(/<script src="..\/..\/src\/scripts\/login\/plugins-modal.js"><\/script>/g, ''))
		.pipe(replace(/<script src="..\/..\/src/g, '<script src="..'))
		.pipe(gulp.dest('./build/html'))
})
gulp.task('reaplaceIndexhtml',function(){
	return gulp.src('./caipiao/index.html')
		.pipe(replace(/href="src\//ig,'href="'))
		.pipe(replace(/<script src="src\/scripts\/login\/jquery.min.js"><\/script>/,'<script src="scripts\/xeplugins.min.js"><\/script>'))
		.pipe(replace(/<script src="src\/scripts\/login\/common.js"><\/script>/,''))
		.pipe(replace(/<script src="src\/scripts\/login\/supersized.3.2.7.min.js"><\/script>/,''))
		.pipe(replace(/<script src="src\/scripts\/login\/supersized-init.js"><\/script>/,''))
		.pipe(replace(/<script src="src\/scripts\/login\/jquery.validate.min.js\?var1.14.0"><\/script>/,''))
		.pipe(gulp.dest('./build'))
})
/*gulp.task('reaplaceCss',function(){
	return gulp.src('./caipiao/src/styles/common.css')
		.pipe(replace(/..\/../ig,'.\/..'))
		.pipe(gulp.dest('./build/styles/cssrename'))
})*/

gulp.task('htmlmins', function() {
  return gulp.src('./caipiao/src/tpl/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./build/html'))
});

/*gulp.task("copy",function(){
	var paths = [
		{src: "./caipiao/src/styles/*.css",dest:"./build/styles/"},
		// {src: "./caipiao/index.html",dest:"./build/"},
		{src: "./caipiao/src/scripts/*.js",dest:"./build/scripts/"}
	]
	return copy2(paths)
})*/
gulp.task('jsmin0',function(){
	return gulp.src(['./caipiao/src/scripts/*.js'])
	.pipe(uglify())
	// .pipe(rename({suffix : '.min'}))
    .pipe(gulp.dest('./build/scripts/'))
});
gulp.task('jsmin',function(){
	return gulp.src(['./caipiao/src/scripts/jquery.min.js','./caipiao/src/scripts/login/*.js'])
	.pipe(concat('xeplugins.js'))
	.pipe(uglify())
	.pipe(rename({suffix : '.min'}))
    .pipe(gulp.dest('./build/scripts/'))
});





// gulp.task('server', shell.task('ps -ef |grep "node app.js"|grep -Ev "grep"|awk \'{print $2}\'|xargs kill -9 && node app.js'));

//监控任务
gulp.task('watch',function(){
	gulp.watch('./caipiao/src/scripts/login/**/*.js',['jsmin']);
	gulp.watch('./caipiao/tpl/**/*.html',['htmlmins']);
})


//默认任务
gulp.task('default',['replaceHtmlStylePaths','cssmin0','reaplaceIndexhtml','jsmin0','concatCommoncss','imagemins','jsmin','cleanFile'])
